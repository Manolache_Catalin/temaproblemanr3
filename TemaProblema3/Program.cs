﻿using System;

namespace TemaProblema3
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabile de baza
            Console.WriteLine("Introduceti n");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            int[] c = new int[0];
            int i = 0;
            //preluarea datelor
            while (i < v.Length)
            {
                v[i] = int.Parse(Console.ReadLine());
                i++;
            }
            //preluare a,b
            Console.WriteLine("Introduceti a");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti b");
            int b = int.Parse(Console.ReadLine());
            i = 0;
            //control max numar
            if (a > b)
            {
                int temp = a;
                a = b;
                b = temp;

            }
            //cliclu pentru operatii
            while (i < v.Length)
            {
                //statement de control
                if(v[i]<a || v[i] > b)
                {
                    //marim dimensiunea vectorului
                    Array.Resize<int>(ref c, c.Length + 1);
                    c[c.Length - 1] = v[i];                
                }
                i++;
            }
            //afisez numarul de elemente care respecta conditiile precizate in problema
            Console.WriteLine("Numarul de elemente este:" + c.Length);
        }
    }
}
